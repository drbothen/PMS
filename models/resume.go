package models

import (
	"encoding/json"
	"os"
)

// TODO: Add godoc style documentation
type Resume struct {
	Basics       Basics         `json:"basics"`
	Work         []Work         `json:"work"`
	Volunteer    []Volunteer    `json:"volunteer"`
	Education    []Education    `json:"education"`
	Awards       []Awards       `json:"awards"`
	Publications []Publications `json:"publications"`
	Skills       []Skills       `json:"skills"`
	Languages    []Languages    `json:"languages"`
	Interests    []Interests    `json:"interests"`
	References   []References   `json:"references"`
}

// TODO: Add godoc style documentation
type Basics struct {
	Name     string     `json:"name"`
	Label    string     `json:"label"`
	Picture  string     `json:"picture"`
	Email    string     `json:"email"`
	Phone    string     `json:"phone"`
	Website  string     `json:"website"`
	Summary  string     `json:"summary"`
	Location Location   `json:"location"`
	Profiles []Profiles `json:"profiles"`
}

// TODO: Add godoc style documentation
type Location struct {
	Address     string `json:"address"`
	PostalCode  string `json:"postalCode"`
	City        string `json:"city"`
	CountryCode string `json:"countryCode"`
	Region      string `json:"region"`
}

// TODO: Add godoc style documentation
type Profiles struct {
	Network  string `json:"network"`
	Username string `json:"username"`
	URL      string `json:"url"`
}

// TODO: Add godoc style documentation
type Work struct {
	Company    string   `json:"company"`
	Position   string   `json:"position"`
	Website    string   `json:"website"`
	StartDate  string   `json:"startDate"`
	EndDate    string   `json:"endDate"`
	Summary    string   `json:"summary"`
	Highlights []string `json:"highlights"`
}

// TODO: Add godoc style documentation
type Volunteer struct {
	Organization string   `json:"organization"`
	Position     string   `json:"position"`
	Website      string   `json:"website"`
	StartDate    string   `json:"startDate"`
	EndDate      string   `json:"endDate"`
	Summary      string   `json:"summary"`
	Highlights   []string `json:"highlights"`
}

// TODO: Add godoc style documentation
type Education struct {
	Institution string   `json:"institution"`
	Area        string   `json:"area"`
	StudyType   string   `json:"studyType"`
	StartDate   string   `json:"startDate"`
	EndDate     string   `json:"endDate"`
	Gpa         string   `json:"gpa"`
	Courses     []string `json:"courses"`
}

// TODO: Add godoc style documentation
type Awards struct {
	Title   string `json:"title"`
	Date    string `json:"date"`
	Awarder string `json:"awarder"`
	Summary string `json:"summary"`
}

// TODO: Add godoc style documentation
type Publications struct {
	Name        string `json:"name"`
	Publisher   string `json:"publisher"`
	ReleaseDate string `json:"releaseDate"`
	Website     string `json:"website"`
	Summary     string `json:"summary"`
}

// TODO: Add godoc style documentation
type Skills struct {
	Name     string   `json:"name"`
	Level    string   `json:"level"`
	Keywords []string `json:"keywords"`
}

// TODO: Add godoc style documentation
type Languages struct {
	Name  string `json:"name"`
	Level string `json:"level"`
}

// TODO: Add godoc style documentation
type Interests struct {
	Name     string   `json:"name"`
	Keywords []string `json:"keywords"`
}

// TODO: Add godoc style documentation
type References struct {
	Name      string `json:"name"`
	Reference string `json:"reference"`
}

// TODO: Add godoc style documentation
func LoadResumeFile(path string) (Resume, error) {
	// Open Resume stored in .json file
	jr, err := os.Open(path)
	// TODO: add custom cant open file error?
	if err != nil {
		return Resume{}, err
	}
	var r Resume // instantiate a Resume struct
	// Pares the json into the Resume struct
	if err := json.NewDecoder(jr).Decode(&r); err != nil {
		return Resume{}, err
	}
	return r, err
}

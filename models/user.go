package models

import (
	"errors"
	"time"
	"github.com/astaxie/beego/orm"
	"golang.org/x/crypto/bcrypt"
	"github.com/astaxie/beego"
	"fmt"
)

var (
	UserList map[int64]*User
)

func init() {
	UserList = make(map[int64]*User)
	u := User{
		Id: 11111,
		Username: beego.AppConfig.String("username"),
		Password: beego.AppConfig.String("password"),
		Role: &Roles{
			Id: 1,
			Name: "Admin",
		},
		Profile: &Profile{
			Id: 11111,
			Gender:"male",
			Age: 30,
			Address: "USA",
			Email: beego.AppConfig.String("email")},
	}
	u.Insert()
}


// User struct for ORM
type User struct {
	Id            int64	`json:"id"`
	Username      string    `orm:"unique"form:"username"valid:"Required"json:"username"`
	Password      string    `form:"password"valid:"Required;MinSize(15)"json:"-"`
	Repassword    string    `orm:"-" form:"Repassword" valid:"Required"json:"-"`
	Lastlogintime time.Time `orm:"type(datetime);null" form:"-"json:"lastlogintime"`
	Created       time.Time `orm:"auto_now_add;type(datetime)" form:"-"json:"created"`
	Updated       time.Time `orm:"auto_now;type(datetime)" form:"-"json:"updated"`
	Posts         []*Post   `orm:"reverse(one)"json:"posts"`
	Role          *Roles    `orm:"reverse(one)"json:"role"` // OneToOne Relationship (reverse)
	Profile  	  *Profile  `orm:"reverse(one)"json:"profile"` // OneToOne Relationship (reverse)
}

// User profile struct
type Profile struct {
	Id int64 `json:"-"`
	Gender  string `json:"gender"`
	Age     int `json:"age"`
	Address string `json:"address"`
	Email   string `orm:"unique"json:"email"`
	User *User `orm:"rel(one)"json:"-"` // Reverse relationship (optional)
}

type LoginToken struct {
	User  User `json:"user"`
	Token string      `json:"token"`
	Expires  int64 `json:"expires"`
}

// TODO: review for removal
// Generated Function
func AddUser(u User) int64 {
	//u.Id = "user_" + strconv.FormatInt(time.Now().UnixNano(), 10)
	u.Id = time.Now().UnixNano()
	UserList[u.Id] = &u
	return u.Id
}

func GetUser(uid int64) (u *User, err error) {
	fmt.Println(UserList[uid])
	if u, ok := UserList[uid]; ok {
		return u, nil
	}
	return nil, errors.New("user not exists")
}


func GetAllUsers() map[int64]*User {
	return UserList
}

func UpdateUser(uid int64, uu *User) (a *User, err error) {
	if u, ok := UserList[uid]; ok {
		if uu.Username != "" {
			u.Username = uu.Username
		}
		if uu.Password != "" {
			u.Password = uu.Password
		}
		if uu.Profile.Age != 0 {
			u.Profile.Age = uu.Profile.Age
		}
		if uu.Profile.Address != "" {
			u.Profile.Address = uu.Profile.Address
		}
		if uu.Profile.Gender != "" {
			u.Profile.Gender = uu.Profile.Gender
		}
		if uu.Profile.Email != "" {
			u.Profile.Email = uu.Profile.Email
		}
		return u, nil
	}
	return nil, errors.New("User Not Exist")
}

func Login(username, password string) bool {
	for _, u := range UserList {
		if u.Username == username && u.Password == password {
			return true
		}
	}
	return false
}

func DeleteUser(uid int64) {
	delete(UserList, uid)
}


// User database CRUD methods

// Insert into user database
func (u *User) Insert() error {
	// for use when there is a database
	//if _, err := orm.NewOrm().Insert(u); err != nil {
	//	return err
	//}
	//return nil

	// in memory database
	u.Id = time.Now().UnixNano()
	UserList[u.Id] = u
	return nil
}

// Read from database
func (u *User) Read(fields ...string) error {
	if err := orm.NewOrm().Read(u, fields...); err != nil {
		return err
	}

	return nil
}

// ReadOrCreate function
func (u *User) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
	return orm.NewOrm().ReadOrCreate(u, field, fields...)
}

// Update function
func (u *User) Update(fields ...string) error {
	// arguments list: (fields ...string)
	if _, err := orm.NewOrm().Update(u, fields...); err != nil {
		return err
	}
	return nil
}

// Delete user
func (u *User) Delete() error {
	if _, err := orm.NewOrm().Delete(u); err != nil {
		return err
	}
	return nil
}

// HashPassword - needs to be called before flushing passwords to database
func (u *User) HashPassword() error {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	u.Password = string(hash[:])
	return nil
}

// CheckPassword : checks password entered by a user
func (u *User) CheckPassword(password string) bool {
	// check password
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	if err != nil {
		return false
	}
	return true
}

// Users : List all users in the database
//func Users() orm.QuerySeter {
//	var table User
//	return orm.NewOrm().QueryTable(table).OrderBy("-Id")
//}

// Authenticate user
func Authenticate(username string, password string) (*User, bool) {
	for _, u := range UserList {
		if u.Username == username {
			if ok := u.CheckPassword(password); ok{
				return u, true
			} else {
				return u, false
			}
		}
	}
	return &User{}, false
}

package models

import (
	"time"
	"github.com/astaxie/beego/orm"
)

type Post struct {
	Id         int64  `json:"id"`
	Title      string `orm:"unique" json:"title" form:"title" binding:"required"`
	Content    string `json:"content"`
	Markdown   string `json:"markdown" form:"markdown"`
	Slug       string `orm:"unique" json:"slug"`
	User       *User  `orm:"rel(one)" json:"author"`  // Author was original name
	Excerpt    string `json:"excerpt"`
	Viewcount  uint   `json:"viewcount"`
	Published  bool   `json:"-"`
	Created    time.Time  `orm:"auto_now_add;type(datetime)" json:"created"`
	Updated    time.Time `orm:"auto_now;type(datetime)" json:"updated"`
}


// Insert into Post model
func (p *Post) Insert() error {
	if _, err := orm.NewOrm().Insert(p); err != nil {
		return err
	}
	return nil
}

// Read from database
func (p *Post) Read(fields ...string) error {
	if err := orm.NewOrm().Read(p, fields...); err != nil {
		return err
	}

	return nil
}

// ReadOrCreate function
func (p *Post) ReadOrCreate(field string, fields ...string) (bool, int64, error) {
	return orm.NewOrm().ReadOrCreate(p, field, fields...)
}

// Update function
func (p *Post) Update(fields ...string) error {
	if _, err := orm.NewOrm().Update(p, fields...); err != nil {
		return err
	}
	return nil
}

// Delete Post
func (p *Post) Delete() error {
	if _, err := orm.NewOrm().Delete(p); err != nil {
		return err
	}
	return nil
}


// Posts : List all users in the database
func Posts() orm.QuerySeter {
	var table Post
	return orm.NewOrm().QueryTable(table).OrderBy("-Id")
}



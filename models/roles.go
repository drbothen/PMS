package models

// Roles : struct for user roles
type Roles struct {
	Id   int64 `json:"-"`
	Name string `orm:"unique"json:"name"`
	User *User  `orm:"rel(one)"json:"-"` // Reverse relationship (optional)
}


package models

import(
	"github.com/astaxie/beego/orm"

	// init for sqlite
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	// register models
	orm.RegisterModel(new(Post))  // Register the Post Model

}

package main

import (
	_ "repo.theoremforge.com/me/PMS/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
	"github.com/casbin/casbin"
	"repo.theoremforge.com/me/PMS/utilities"
)

var (
	version = "Place Holder"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
		beego.InsertFilter("*", beego.BeforeRouter,cors.Allow(&cors.Options{
			AllowAllOrigins: true, //TODO(jmagady): what does this do?
			AllowOrigins: []string{"*"},  // Sets the origins that the server will accept
			AllowMethods: []string{"GET", "POST"},  // Sets the methods the server will accept
			AllowHeaders: []string{"Authorization"}, // Sets the headers that the server will accept
			ExposeHeaders: []string{"Content-Length"},  //TODO(jmagady): what does this do?
			AllowCredentials: true,
		}))
	}
	
	beego.SetStaticPath("/main.dart.js", "static/app/main.dart.js")
	beego.SetStaticPath("/styles.css", "static/app/styles.css")
	beego.SetStaticPath("/fonts", "static/fonts")

	beego.InsertFilter("*", beego.BeforeRouter, utilities.NewAuthorizer(casbin.NewEnforcer("conf/auth_model.conf", "conf/auth_policy.csv")))
	beego.Run()
}

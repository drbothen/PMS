package main

import (
	"github.com/astaxie/beego"
	_ "repo.theoremforge.com/jmagady/mypms/routers"
	_ "repo.theoremforge.com/jmagady/mypms/views"
)

func main() {
	beego.SetStaticPath("/img", "static/img")
	beego.SetStaticPath("/css", "static/css")
	beego.SetStaticPath("/js", "static/js")
	beego.SetStaticPath("/json", "static/json")
	beego.DelStaticPath("/static") // delete old static
	beego.Run()
}

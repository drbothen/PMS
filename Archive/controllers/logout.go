package controllers

type LogoutController struct {
	MainController
}

func (c *LogoutController) Get() {
	c.DelSession("mesite")
	c.Redirect("/", 302)
}

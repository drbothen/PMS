package controllers

import (
	"github.com/astaxie/beego"
	"github.com/drbothen/go-JSONResume"
	"log"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	ses := c.GetSession("mesite")
	if ses != nil {
		c.Data["insession"] = true
		c.Data["NavTemplate"] = "root"
		c.Data["first"] = "Admin"
	}

	jr, err := JSONResume.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		log.Fatalln(err)
	}
	c.Data["resume"] = jr // assign the parsed resume
	c.TplName = "content/resume.html"
}

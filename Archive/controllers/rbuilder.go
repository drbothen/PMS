package controllers

type RBuilderController struct {
	MainController
}

func (c *RBuilderController) Get() {

	ses := c.GetSession("mesite")
	if ses != nil {
		c.Data["insession"] = true
		c.Data["NavTemplate"] = "rbuilder"
		c.Data["first"] = "Admin"
	} else {
		c.Redirect("/login", 302)
	}

	c.TplName = "content/rbuilder.html"
}

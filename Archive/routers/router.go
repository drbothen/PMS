package routers

import (
	"github.com/astaxie/beego"
	"repo.theoremforge.com/jmagady/mypms/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/login", &controllers.LoginController{})
	beego.Router("/logout", &controllers.LogoutController{})
	beego.Router("/rbuilder", &controllers.RBuilderController{})
}

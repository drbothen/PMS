package models

type LoginForm struct {
	Username string `form:"username"valid:"Required;Email"`
	Password string `form:"password"valid:"Required"`
}

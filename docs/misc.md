### Helpful additions to dev environment
#### Better git log
Are tired of this same old and bored git log screen?
![oldgitlog](img/oldgitlog.png)

How about this one instead?
![newgitlog](img/newgitlog.png)
```git
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
```
That may be a little long to type in each and everytime, so lets make an
alias.
```git
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
```

Now when you need to see your log, you can just type the below
```git
git lg
```

Or, if you want to see the lines that changed
```git
git lg -p
```
![newgitlogp](img/newgitlogp.png)
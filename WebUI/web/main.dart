import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Personal_Management_System/app_component.dart';
//import 'package:Personal_Management_System/in_memory_data_service.dart';
import 'package:http/http.dart';
import 'package:http/browser_client.dart';
import 'package:Personal_Management_System/src/services/services.dart';
import 'package:w_transport/w_transport.dart' as transport;  // import as transport to avoid conflicts
import 'package:w_transport/browser.dart';
import 'package:Personal_Management_System/src/interceptors/auth_interceptor.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';
import 'dart:html' as html;  // import as HTML to avoid conflicts

/// Create logger
const String APP_NAME = "PMS";
final bool _debugMode = html.window.location.host.contains('localhost');

final LoggerService _log = new LoggerService(appName: APP_NAME, debugMode: _debugMode);


// Create http client for use through out the app
transport.HttpClient client = new transport.HttpClient(transportPlatform: browserTransportPlatform)
  ..addInterceptor(new AuthInterceptor())  // Add auth interceptor to append JWT
  ..baseUri = Uri.parse("http://localhost:8000/");  // Set baseURI for the app

void main() {
  //_client.baseUri = Uri.parse("http://localhost:8000/");
  bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    PMS_PROVIDERS,

    // Remove next line in production
    //provide(LocationStrategy, useClass: HashLocationStrategy),
    provide(LocationStrategy, useClass: PathLocationStrategy),
    //provide(Client, useClass: InMemoryDataService),
    // Using a real back end?
    // Import browser_client.dart and change the above to:
    [provide(Client, useFactory: () => new BrowserClient(), deps: [])], // TODO(jmagady): no longer used - need to update auth_service
    // provide logger service
    [provide(
        LoggerService,
        useValue: _log,
        deps: []
    )],
    // provide http client
    [provide(
        transport.HttpClient,
        useValue: client,
        deps: [])],
    //[provide(HttpRequest, useFactory: () => new HttpRequest(), deps: [])],
  ]);
}
/*
import 'package:http/browser_client.dart';

void main() {
  bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    // Remove next line in production
    provide(LocationStrategy, useClass: HashLocationStrategy),
    provide(BrowserClient, useFactory: () => new BrowserClient(), deps: [])
  ]);
}
*/

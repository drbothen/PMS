import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/skills_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-skills',
  templateUrl: 'skills_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['skills_component.css'],
  pipes: const [COMMON_PIPES],
  providers: const [SkillsService],
)
class SkillsComponent implements OnInit {
  final LoggerService _log;

  JsonObject skills; // Profile

  SkillsService _skillsService; // Profile Services

  SkillsComponent(
      this._skillsService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::SkillsComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    skills = await _skillsService.getSkills();
    //workexp.forEach((element) => print(element.summary));
  }
}
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Personal_Management_System/src/resume/workexperience/workexperience_component.dart';
import 'package:Personal_Management_System/src/resume/volunteerwork/volunteerwork_component.dart';
import 'package:Personal_Management_System/src/resume/education/education_component.dart';
import 'package:Personal_Management_System/src/resume/about/about_component.dart';
import 'package:Personal_Management_System/src/resume/awards/awards_component.dart';
import 'package:Personal_Management_System/src/resume/publications/publications_component.dart';
import 'package:Personal_Management_System/src/resume/interests/interests_component.dart';
import 'package:Personal_Management_System/src/resume/references/references_component.dart';
import 'package:Personal_Management_System/src/resume/skills/skills_component.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';




@Component(
  selector: 'my-resume',
  templateUrl: 'resume_component.html',
  directives: const [
    CORE_DIRECTIVES, 
    ROUTER_DIRECTIVES, 
    WorkExperienceComponent,
    VolunteerWorkComponent,
    EducationComponent,
    AboutComponent,
    AwardsComponent,
    PublicationsComponent,
    InterestsComponent,
    ReferencesComponent,
    SkillsComponent
  ],
  pipes: const [COMMON_PIPES],
)
class ResumeComponent {
  final LoggerService _log;

  ResumeComponent(
      LoggerService this._log
      ){
    _log.info("$runtimeType::ResumeComponent()");
  }
}

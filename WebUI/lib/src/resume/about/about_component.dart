import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/basics_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-about',
  templateUrl: 'about_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['about_component.css'],
  pipes: const [COMMON_PIPES],
  providers: const [BasicsService],
)
class AboutComponent implements OnInit {
  final LoggerService _log;
  JsonObject about; // Profile

  BasicsService _basicsService; // Profile Services

  AboutComponent(
      this._basicsService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::AboutComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    about = await _basicsService.getBasics();
    //workexp.forEach((element) => print(element.summary));
  }
}
import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/education_service.dart';
import 'package:Personal_Management_System/src/pipes/string_to_date_pipe.dart';
import 'package:Personal_Management_System/src/pipes/date_difference_pipe.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-edu',
  templateUrl: 'education_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['education_component.css'],
  pipes: const [
    COMMON_PIPES,
    StringToDatePipe,
    DateDifferencePipe
  ],
  providers: const [EducationService],
)
class EducationComponent implements OnInit {
  final LoggerService _log;

  JsonObject education; // Profile

  EducationService _educationService; // Profile Services

  EducationComponent(
      this._educationService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::EducationComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    education = await _educationService.getedu();
    //workexp.forEach((element) => print(element.summary));
  }
}
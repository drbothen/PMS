import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/awards_service.dart';
import 'package:Personal_Management_System/src/pipes/string_to_date_pipe.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-awards',
  templateUrl: 'awards_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['awards_component.css'],
  pipes: const [COMMON_PIPES, StringToDatePipe],
  providers: const [AwardsService],
)
class AwardsComponent implements OnInit {
  final LoggerService _log;

  JsonObject awards; // Profile

  AwardsService _AwardsService; // Profile Services

  AwardsComponent(
      this._AwardsService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::AwardsComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    awards = await _AwardsService.getAwards();
    //workexp.forEach((element) => print(element.summary));
  }
}
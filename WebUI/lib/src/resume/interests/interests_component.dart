import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/interests_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-interests',
  templateUrl: 'interests_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['interests_component.css'],
  pipes: const [COMMON_PIPES],
  providers: const [InterestsService],
)
class InterestsComponent implements OnInit {
  final LoggerService _log;

  JsonObject interests; // Profile

  InterestsService _interestsService; // Profile Services

  InterestsComponent(
      this._interestsService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::InterestsComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    interests = await _interestsService.getInterests();
    //workexp.forEach((element) => print(element.summary));
  }
}
import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/volunteer_service.dart';
import 'package:Personal_Management_System/src/pipes/string_to_date_pipe.dart';
import 'package:Personal_Management_System/src/pipes/date_difference_pipe.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-volnwork',
  templateUrl: 'volunteerwork_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['volunteerwork_component.css'],
  pipes: const [
    COMMON_PIPES,
    StringToDatePipe,
    DateDifferencePipe
  ],
  providers: const [VolunteerWorkService],
)
class VolunteerWorkComponent implements OnInit {
  final LoggerService _log;

  JsonObject volnexp; // Profile

  VolunteerWorkService _volunteerWorkService; // Profile Services

  VolunteerWorkComponent(
      this._volunteerWorkService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::VolunteerWorkComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    volnexp = await _volunteerWorkService.getVolunteer();
    //workexp.forEach((element) => print(element.summary));
  }
}
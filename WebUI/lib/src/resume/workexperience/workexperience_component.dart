import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/workexperience_service.dart';
import 'package:Personal_Management_System/src/pipes/string_to_date_pipe.dart';
import 'package:Personal_Management_System/src/pipes/date_difference_pipe.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';

@Component(
  selector: 'my-workexp',
  templateUrl: 'workexperience_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['workexperience_component.css'],
  pipes: const [
    COMMON_PIPES,
    StringToDatePipe,
    DateDifferencePipe
  ],
  providers: const [WorkExperienceService],
)
class WorkExperienceComponent implements OnInit {
  final LoggerService _log;
  JsonObject workexp; // Profile

  WorkExperienceService _workExperienceService; // Profile Services

  WorkExperienceComponent(
      this._workExperienceService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::WorkExperienceComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    workexp = await _workExperienceService.getWork();
    //workexp.forEach((element) => print(element.summary));
  }
}
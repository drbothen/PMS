import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/references_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-refs',
  templateUrl: 'references_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['references_component.css'],
  pipes: const [COMMON_PIPES],
  providers: const [ReferencesService],
)
class ReferencesComponent implements OnInit {
  final LoggerService _log;

  JsonObject references; // Profile

  ReferencesService _referencesService; // Profile Services

  ReferencesComponent(
      this._referencesService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::ReferencesComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    references = await _referencesService.getRefs();
    //workexp.forEach((element) => print(element.summary));
  }
}
import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/publications_service.dart';
import 'package:Personal_Management_System/src/pipes/string_to_date_pipe.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-pubs',
  templateUrl: 'publications_component.html',
  //template: '<div>test</div>',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  styleUrls: const ['publications_component.css'],
  pipes: const [COMMON_PIPES, StringToDatePipe],
  providers: const [PublicationsService],
)
class PublicationsComponent implements OnInit {
  final LoggerService _log;

  JsonObject publications; // Profile

  PublicationsService _publicationsService; // Profile Services

  PublicationsComponent(
      this._publicationsService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::PublicationsComponent()");
  }

  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    publications = await _publicationsService.getPubs();
    //workexp.forEach((element) => print(element.summary));
  }
}
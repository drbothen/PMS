import 'dart:core';
import 'package:angular/angular.dart';

/*
 * Convert String date to DateTime type
 * Takes a date string.
 * Usage:
 *   value | stringtoDate
 * Example:
 *   {{  |  stringtoDate}}
 *   formats to: 
 */
@Pipe('stringtoDate')
class StringToDatePipe extends PipeTransform {
  DateTime transform(String value) {
    return DateTime.parse(value);
  }
}

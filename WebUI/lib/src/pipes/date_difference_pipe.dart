import 'dart:core';
import 'package:angular/angular.dart';

/*
 * Calculates the difference between dates
 * Takes a datetime.
 * Usage:
 *   value | dateDifference:end
 * Example:
 *   {{  |  dateDifference: }}
 *   formats to:
 */
@Pipe('dateDifference')
class DateDifferencePipe extends PipeTransform {
  String transform(String start, String end) {
    DateTime startP = DateTime.parse(start);
    DateTime endP = DateTime.parse(end);
    Duration difference = startP.difference(endP);
    int secs = difference.inSeconds;
    int mins = difference.inMinutes;
    int hours = difference.inHours;
    int days = difference.inDays;
    int months = (days/31).round();
    int years = (months/12).round();
    months = (months%12).round();
    days = (days%31).round();
    hours = (hours%24).round();
    mins = (mins%60).round();
    secs = (secs%60).round();
    String message;
    if (years > 1 && months > 1){
      message = "${years} years, ${months} months";
    } else if (years != 0 && months != 0 && years < 2 && months < 2){
      message = "${years} year, ${months} month";
    } else if (years != 0 && years < 2){
      message = "${years} year";
    } else if (years > 1){
      message = "${years} years";
    } else if (years == 0 && months < 2){
      message = "${months} month";
    } else if (years == 0 && months > 1){
      message = "${months} months";
    }
    return message;
  }
}
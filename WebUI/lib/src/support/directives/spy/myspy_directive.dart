import 'package:angular/angular.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';

int _nextId = 1;

// Spy on any element to which it is applied.
// Usage: <div mySpy>...</div>
@Directive(selector: '[mySpy]')
class SpyDirective implements OnInit, OnDestroy {
  final LoggerService _logger;

  SpyDirective(this._logger);

  ngOnInit() => _logIt('onInit');

  ngOnDestroy() => _logIt('onDestroy');

  _logIt(String msg) => _logger.info('Spy #${_nextId++} $msg');
}
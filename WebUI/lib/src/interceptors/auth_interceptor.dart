import 'dart:async';
import 'package:w_transport/w_transport.dart' as transport;
import 'dart:html';

class AuthInterceptor extends transport.HttpInterceptor {
  @override
  Future<transport.RequestPayload> interceptRequest(transport.RequestPayload payload) async {
    if (window.sessionStorage.containsKey("id_token")) {  // check for a JWT token
      payload.request.headers['Authorization'] = window.sessionStorage["id_token"];  // add JWT token to header
    }
    return payload;  // return payload
  }
}
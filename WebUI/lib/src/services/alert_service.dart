import 'package:angular/angular.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';
import 'dart:async';

@Injectable()
class AlertService {
  final LoggerService _log;

  Stream onAlert;
  StreamController controller;

  AlertService(
      LoggerService this._log
      ){
    _log.info("$runtimeType::AlertService()");
    this.controller = new StreamController.broadcast();
    this.onAlert = controller.stream;
  }

  // Add alert to the service
  addAlert(msg) async {
    controller.add(msg);
    _log.info("$runtimeType::addAlert()::$msg");
  }

}
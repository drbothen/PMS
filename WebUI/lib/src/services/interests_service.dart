import 'dart:async';
import 'package:angular/angular.dart';
import 'package:http/http.dart';
import 'package:json_object/json_object.dart';
import 'package:w_transport/w_transport.dart' as transport;
import 'package:Personal_Management_System/src/services/logger_service.dart';

@Injectable()
class InterestsService {
  final LoggerService _log;
  final transport.HttpClient _http;
  final String _url = "v1/resume/interests";

  InterestsService(
      this._http,
      LoggerService this._log
      ){
    _log.info("$runtimeType::InterestsService()");
  }

  // TODO(jmagady): move to a class that this class implements or extends
  Future<JsonObject> getInterests() async {
    _log.info("$runtimeType::getInterests()");
    try {
      final req = _http.newRequest()..appendToPath(_url);  // create a new request
      final response = await req.get();  // execute a get request and wait for response
      JsonObject data = _extractData(response); // Parse to Object TODO(jmagady): Look into removing this? use .asJson
      return data; // return extracted data
    } catch (e) {
      throw _handleError(e);
    }
  }

  // TODO(jmagady): move to a class that this class implements or extends
  dynamic _extractData(transport.Response resp) {
    _log.info("$runtimeType::_extractData()");
    JsonObject data = new JsonObject.fromJsonString(resp.body.asString());
    if (data.success){
      return data['data'];
    }
  }

  Exception _handleError(dynamic e) {
    _log.severe("$runtimeType::_handleError()");
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }
}


import 'dart:async';
import 'package:intl/intl.dart';

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:http/http.dart';
import 'package:json_object/json_object.dart';
import 'storage_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';
import 'package:Personal_Management_System/src/services/alert_service.dart';



@Injectable()
class AuthService {
  final LoggerService _log;
  final Client _http;
  String _url = "http://localhost:8000/v1/user/login";
  //String _url = "v1/resume/awards";

  AlertService _alertservice;

  StorageService _storageservice;
  Router _router;

  Stream onLogin;
  StreamController controller;
  //User _user;

  //Stream<bool> get onLogin => _onLogin.stream;


  AuthService(
      this._http,
      this._storageservice,
      this._router,
      LoggerService this._log,
      AlertService this._alertservice
      ){
    _log.info("$runtimeType::AuthService()");
    this.controller = new StreamController.broadcast();
    this.onLogin = controller.stream;
  }


  // Login to the app
 login(String username, String password) async {
   _log.info("$runtimeType::login()");
   JsonObject data = await _http.post(_url, headers: {}, body: {"username":username, "password":password})
       .then(_extractData);
   if (data.success) {  // if the api call returns success = true
     DateTime expires = new DateTime.fromMillisecondsSinceEpoch(data.data.expires);
     var format = new DateFormat("yMd");
     var dateString = format.format(expires);
     //print(dateString);
     this._storageservice.setSession(data);
     controller.add(this.isLoggedIn());
     _alertservice.addAlert({
       'type': 'success',
       'msg': 'Well done! You successfully read this important alert message.'
     });
     this._router.navigateByUrl("/");
   } else {
     _alertservice.addAlert({
       'type': 'danger',
       'msg': 'Oh snap! Change a few things up and try submitting again.'
     });

   }

  }

  logout() async {
    _log.info("$runtimeType::logout()");
   _storageservice.removeSession();  // Clears all contents of sessionstorage
   controller.add(this.isLoggedIn());
  }

  // extract data from the json repsonse
  dynamic _extractData(Response resp) {
    _log.info("$runtimeType::_extractData()");
    JsonObject data = new JsonObject.fromJsonString(resp.body);
    return data;
  }

  Exception _handleError(dynamic e) {
    _log.severe("$runtimeType::_extractData()");
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }

  // check if we are logged in
  bool isLoggedIn() {
    _log.info("$runtimeType::isLoggedIn()");
   dynamic currentExp = _storageservice.getExpiration();  // returns false if no expiration
   if (currentExp != false) {
     final expiration = new DateTime.fromMillisecondsSinceEpoch(
         int.parse(currentExp));
     final currentTime = new DateTime.now();
     return expiration.isAfter(currentTime);
   }
   return false;
  }

  // check if we are logged out
  bool isLoggedOut() {
    _log.info("$runtimeType::isLoggedOut()");
    return !this.isLoggedIn();
  }
}


//login(email:string, password:string ) {
//return this.http.post<User>('/api/login', {email, password})
//// this is just the HTTP call,
//// we still need to handle the reception of the token
//    .shareReplay();
//}


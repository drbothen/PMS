import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:http/http.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';

@Injectable()
class StorageService {
  final LoggerService _log;

  StorageService(
      LoggerService this._log
      ){
    _log.info("$runtimeType::StorageService()");
  }

  // Sets session details in sessionStorage
  void setSession(JsonObject data) {
    _log.info("$runtimeType::setSession()");
    JsonObject session = data["data"];
    window.sessionStorage["id_token"] = session["token"];
    window.sessionStorage["profile"] = session["user"];
    window.sessionStorage["expires"] = session["expires"];
  }

  // Gets the expires string from sessionsStorage
  dynamic getExpiration(){
    _log.info("$runtimeType::getExpiration()");
    if (window.sessionStorage.containsKey("id_token")){
      return window.sessionStorage["expires"];
    }
    return false;
  }

  // Clear sessions - Used for logging out.
  void removeSession() {
    _log.info("$runtimeType::removeSession()");
    window.sessionStorage.clear();
  }

  Exception _handleError(dynamic e) {
    _log.severe("$runtimeType::_handleError(()");
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }

  bool haveToken(){
    _log.info("$runtimeType::haveToken()");
    return window.sessionStorage.containsKey("id_token");
  }

  String getToken(){
    _log.info("$runtimeType::getToken()");
    return window.sessionStorage["id_token"];
  }
}


import 'auth_service.dart';
import 'awards_service.dart';
import 'basics_service.dart';
import 'education_service.dart';
import 'interests_service.dart';
import 'language_service.dart';
import 'publications_service.dart';
import 'references_service.dart';
import 'skills_service.dart';
import 'storage_service.dart';
import 'volunteer_service.dart';
import 'wakatime_service.dart';
import 'workexperience_service.dart';
import 'alert_service.dart';

const List<Type> RESUME_PROVIDERS = const [
  AwardsService,
  BasicsService,
  EducationService,
  InterestsService,
  LanguageService,
  PublicationsService,
  ReferencesService,
  SkillsService,
  VolunteerWorkService,
  WorkExperienceService
];

const List<Type> PMS_PROVIDERS = const [
  AuthService,
  StorageService,
  AlertService,
];
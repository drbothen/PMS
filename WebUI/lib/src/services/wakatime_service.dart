import 'dart:async';

import 'package:angular/angular.dart';
import 'package:http/http.dart';
import 'package:json_object/json_object.dart';


@Injectable()
class WakaTimeService {
  final Client _http;
  String _url = "https://wakatime.com/share/@drbothen/96c6b251-f29e-497a-a188-58290a5ff042.json"; // Need to update to your personal link

  WakaTimeService(this._http);


  Future<JsonObject> getWaka() async {
    try {
      final response = await _http.get(_url);
      JsonObject data = _extractData(response);
      return data;
    } catch (e) {
      throw _handleError(e);
    }
  }

  dynamic _extractData(Response resp) {
    JsonObject data = new JsonObject.fromJsonString(resp.body)['Data'];
    return data;
  }

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }
}


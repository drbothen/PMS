import 'dart:async';
import 'dart:js' as js;
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Personal_Management_System/src/services/basics_service.dart';
import 'package:json_object/json_object.dart';
import 'package:Personal_Management_System/src/services/language_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-basics',
  templateUrl: 'basics_component.html',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  pipes: const [COMMON_PIPES],
  providers: const [BasicsService, LanguageService],
)
class BasicsComponent implements OnInit {
  JsonObject basics; // Profile
  JsonObject language; // Languages
  final LoggerService _log;

  BasicsService _basicsService; // Profile Services
  LanguageService _languageService; // Language Services

  BasicsComponent(
      this._basicsService,
      this._languageService,
      LoggerService this._log
      ){
    _log.info("$runtimeType::BasicsComponent()");
  }
  
  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");  // Log Call

    basics = await _basicsService.getBasics();
    language = await _languageService.getlang();
    js.context.callMethod(r'$',['[data-toggle="tooltip"]'])
        .callMethod('tooltip', []); // enables bootstrap tooltip
  }
}

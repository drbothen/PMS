import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Personal_Management_System/src/services/auth_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';



@Component(
  selector: 'my-nav',
  templateUrl: 'navigation_component.html',
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES],
  pipes: const [COMMON_PIPES],
  //providers: const [AuthService],
)
class NavigationComponent implements OnInit {

  final LoggerService _log;

  Router _router;  // router service
  AuthService _authservice;  // auth service
  var _subscription;


  bool loggedin;  // logged in bool used in template

  NavigationComponent(
      this._router,
      this._authservice,
      LoggerService this._log
      ) {
    _log.info("$runtimeType::NavigationComponent()");

    this._subscription = _authservice.onLogin.listen((status) {
      _log.info("$runtimeType::event fired::{isLoggedin():$status}");
      //print(status);
      loggedin = status;  // set value from auth service.
    });

    //
    this._authservice.controller.add(_authservice.isLoggedIn());
  }
  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
    querySelector('.js-floating-nav-trigger').onClick.listen(toggleDefault);  // powers nav
    //querySelector('#nav-link').onClick.listen(toggleDefault);  // powers nav
  }

  // nav related function. used in toggling
  toggleFloatingMenu() {
    _log.info("$runtimeType::toggleFloatingMenu()");
    querySelector('.js-floating-nav').classes.toggle('is-visible');
    querySelector('.js-floating-nav-trigger').classes.toggle('is-open');
  }

  // nav related function. used in toggling
  toggleDefault(MouseEvent event) {
    _log.info("$runtimeType::toggleDefault()");
    event.preventDefault();
    toggleFloatingMenu();
  }

  // nav related function. used in toggling
  toggleMod(MouseEvent event) {
    _log.info("$runtimeType::toggleMod()");
    toggleFloatingMenu();
  }

  logOut() {
    _log.info("$runtimeType::logOut()");
    this._authservice.logout();
  }

}

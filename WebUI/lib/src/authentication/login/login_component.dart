import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:Personal_Management_System/src/services/auth_service.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';


@Component(
  selector: 'my-login',
  templateUrl: 'login_component.html',
  viewProviders: const [FORM_BINDINGS],
  directives: const [CORE_DIRECTIVES, ROUTER_DIRECTIVES, formDirectives],
  pipes: const [COMMON_PIPES],
  //providers: const [AuthService],
)
class LoginComponent implements OnInit {
  final LoggerService _log;

  String loginurl = "http://localhost:8000/v1/user/login";

  ControlGroup loginForm;
  AuthService _authservice;
      
  LoginComponent(
      this._authservice,
      LoggerService this._log
      ){
    _log.info("$runtimeType::LoginComponent()");
    final builder = new FormBuilder();
    loginForm = builder.group({
      "username": ["", Validators.required],
      "password": ["", Validators.required],
    });
  } // Constructor
  
  Future<Null> ngOnInit() async {
    _log.info("$runtimeType::ngOnInit()");
  }

  login() {
    _log.info("$runtimeType::login()");

   final Map<String,String> val = this.loginForm.value;
   
    if (val["username"] != "" && val["password"] != ""){
      this._authservice.login(val["username"], val["password"]);
    }
  }
}

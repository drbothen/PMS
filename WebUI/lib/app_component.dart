import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';
import 'package:Personal_Management_System/src/basics/basics_component.dart';
import 'package:Personal_Management_System/src/resume/resume_component.dart';
import 'package:Personal_Management_System/src/navigation/navigation_component.dart';
import 'package:Personal_Management_System/src/authentication/login/login_component.dart';
import 'package:Personal_Management_System/src/services/logger_service.dart';
import 'package:Personal_Management_System/src/alerts/alerts_component.dart';


@Component(
  selector: 'my-app',
  templateUrl: 'app_component.html',
  styleUrls: const ['app_component.css'],
  directives: const [ROUTER_DIRECTIVES, BasicsComponent, NavigationComponent, AlertsComponent],
  //providers: const [],
)
@RouteConfig(const [
//  const Route(
//    path: '/home',
//    name: 'Home',
//    component: NavigationComponent,
//    useAsDefault: false
//  ),
  const Route(
      path: '/resume',
      name: 'Resume',
      component: ResumeComponent,
      useAsDefault: true),
  const Route(
      path: '/login',
      name: 'Login',
      component: LoginComponent,
      useAsDefault: false)
])
class AppComponent {
  final LoggerService _log;

  AppComponent(LoggerService this._log) {
    _log.info("$runtimeType::AppComponent()");
  }
}

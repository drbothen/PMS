# Personal Management System (PMS)

PMS is a personal management system to be used to bootstrap your personal brand.
This system was born out of frustration from not being able to find anything
that had all the features I was looking for in a PMS. PMS is used to not only
display your personal credentials and help you stand out in a technical world, but also
provide a platform for personal expression in brand building.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

TODO

## Running the tests

TODO: Explain how to run the automated tests for this system

### End to end tests

TODO: Explain what these tests test and why

```
Give an example
```

### Coding style tests

TDO: Explain what these tests test and why

```
Give an example
```

## Deployment

TODO: Add additional notes about how to deploy this on a live system

## Built With

* [Beego](https://beego.me/) - The web framework used
* [JSON Resume](https://jsonresume.org/) - Format used for resume
* [DEP](https://github.com/golang/dep) - Dependency Management
* [Task](https://github.com/go-task/task) - Task Runner
* [AngularDart](https://webdev.dartlang.org/angular) - Front-end framework

## Contributing

Please read [CONTRIBUTING.md](https://repo.theoremforge.com/me/PMS/blob/develop/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://repo.theoremforge.com/me/PMS/tags).

## Authors

* **Joshua Magady** - *Original Author* - [drbothen](https://github.com/drbothen)
* **Jared Richards** - *Core Contributor*
* **Michael Lester** - *Core Contributor*


## License

TODO

## Acknowledgments

TODO
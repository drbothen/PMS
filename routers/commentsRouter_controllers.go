package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ObjectController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:objectId`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetAwards",
			Router: `/awards`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetAllBasics",
			Router: `/basics`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetLocation",
			Router: `/basics/location`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetProfiles",
			Router: `/basics/profiles`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetEducation",
			Router: `/education`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetInterests",
			Router: `/interests`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetLanguages",
			Router: `/languages`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetPublications",
			Router: `/publications`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetReferences",
			Router: `/references`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetSkills",
			Router: `/skills`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetVolunteer",
			Router: `/volunteer`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:ResumeController"],
		beego.ControllerComments{
			Method: "GetWork",
			Router: `/work`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:uid`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"] = append(beego.GlobalControllerRouter["repo.theoremforge.com/me/PMS/controllers:UserController"],
		beego.ControllerComments{
			Method: "Logout",
			Router: `/logout`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}

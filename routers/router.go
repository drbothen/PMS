// @APIVersion 1.0.0
// @Title Personal Management System API
// @Description This is the documented API for the Personal Management System
// @Contact josh.magady@gmail.com
// @TermsOfServiceUrl https://magady.me
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"repo.theoremforge.com/me/PMS/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/object",
			beego.NSInclude(
				&controllers.ObjectController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
		beego.NSNamespace("/resume",
			beego.NSInclude(
				&controllers.ResumeController{},
			),
		),
	)
	beego.AddNamespace(ns)

	beego.Router("/*", &controllers.AngularController{})

}

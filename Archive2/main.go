package main

import (
	_ "repo.theoremforge.com/jmagady/mypms/routers"

	"github.com/astaxie/beego"
)

var (
	version = "Place Holder"
)

func main() {
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}

	beego.SetStaticPath("/", "static/app/index.html")
	beego.SetStaticPath("/main.dart.js", "static/app/main.dart.js")
	beego.SetStaticPath("/styles.css", "static/app/styles.css")
	beego.Run()
}

package controllers

import (
	"repo.theoremforge.com/me/PMS/models"
	"fmt"
	//"github.com/astaxie/beego/validation"
	"github.com/astaxie/beego/validation"
	"github.com/astaxie/beego"
	"time"
)

type LoginController struct {
	MainController
}


func (c *LoginController) Get() {
	c.TplName = "content/login.html"
}

func (c *LoginController) Post()  {
	c.TplName = "content/login.html" // define template to use

	var loginForm models.LoginForm
	c.ParseForm(&loginForm) // parse login form
	fmt.Println(loginForm) // remove after testing

	flash := beego.NewFlash() // create a new flash
	valid := validation.Validation{} // create a new validation
	if b, err := valid.Valid(&loginForm); err != nil { // validate posted login form
		flash.Error("Input invalid")
		flash.Store(&c.Controller)
		//c.Redirect("/login", 302)
		return
	} else if !b {
		flash.Error("Validation Error")
		flash.Store(&c.Controller)
		//c.Redirect("/login", 302)

		// generate error map
		errormap := []string{}
		for _, err := range valid.Errors {
			errormap = append(errormap, "Validation failed on "+err.Key+": "+err.Message+"\n")
		}
		c.Data["Errors"] = errormap
		return
	}

	// Replace with actually user login code when database implemented
	if beego.AppConfig.String("username") !=  loginForm.Username {
			flash.Error("Account not found")
			flash.Store(&c.Controller)
			return
	} else if beego.AppConfig.String("password") != loginForm.Password {
		flash.Error("Password incorrect")
		flash.Store(&c.Controller)
		return
	}

	// create session
	m := make(map[string]interface{})
	m["name"] = "Admin"
	m["username"] = loginForm.Username
	m["timestamp"] = time.Now()
	c.SetSession("mesite", m)
	c.Redirect("/", 302)
}

package views

import (
	"github.com/astaxie/beego"
	"time"
	"fmt"
)



func init() {
	beego.AddFuncMap("prettydate", func(date string) string {
		layout := "2006-01-02"
		t, _ := time.Parse(layout, date)
		return fmt.Sprintf(t.Format("January 1, 2006"))
	})

	beego.AddFuncMap("timediff", func(start, end string) string {
		layout := "2006-01-02"
		tstart, _ := time.Parse(layout, start)
		tend, _ := time.Parse(layout, end)
		year, month, _, _, _, _ := difftime(tstart, tend)

		var fmtstring string

		switch {
		case (year > 1) && (month > 1):
			fmtstring = fmt.Sprintf("%v years, %v months", year, month)
		case (year != 0) && (month != 0) && (year < 2) && (month < 2):
			fmtstring = fmt.Sprintf("%v year, %v month", year, month)
		case (year != 0) && (year < 2):
			fmtstring = fmt.Sprintf("%v year", year)
		case year > 1:
			fmtstring = fmt.Sprintf("%v years", year)
		case (year == 0) && (month < 2):
			fmtstring = fmt.Sprintf("%v month", month)
		case (year == 0) && (month > 1):
			fmtstring = fmt.Sprintf("%v months", month)
		}

		return fmtstring
	})
}

func difftime(a, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = int(y2 - y1)
	month = int(M2 - M1)
	day = int(d2 - d1)
	hour = int(h2 - h1)
	min = int(m2 - m1)
	sec = int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return
}
package controllers

import "github.com/astaxie/beego"

type AngularController struct {
	beego.Controller
}

func (c *AngularController) Get() {
	c.TplName = "index.html"
	c.Render()
}

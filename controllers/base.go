package controllers

import (
	"github.com/astaxie/beego"
)

type BaseController struct {
	beego.Controller
}

//Response Wrapper
type Response struct {
	Success  bool      `json:"success"`
	Data    interface{} `json:"data"`
}

//Error Response
type ErrResponse struct {
	Errcode int         `json:"errcode"`
	Errmsg  interface{} `json:"errmsg"`
}

package controllers

import (
	"encoding/json"
	"repo.theoremforge.com/me/PMS/models"

	"github.com/astaxie/beego"
	"strconv"
	"fmt"
	"time"
	"repo.theoremforge.com/me/PMS/utilities"
)

var (
	ErrUsrnameIsRegis  = ErrResponse{422002, "username has already been registered"}
	ErrUsrnameOrPasswd = ErrResponse{422003, "incorrect username or password"}
)


// Operations about Users
type UserController struct {
	beego.Controller
}

// @Title CreateUser
// @Description create users
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {int} models.User.Id
// @Failure 403 body is empty
// @router / [post]
func (u *UserController) Post() {
	var user models.User
	json.Unmarshal(u.Ctx.Input.RequestBody, &user)
	uid := models.AddUser(user)
	u.Data["json"] = map[string]int64{"uid": uid}
	u.ServeJSON()
}

// @Title GetAll
// @Description get all Users
// @Success 200 {object} models.User
// @router / [get]
func (u *UserController) GetAll() {
	users := models.GetAllUsers()
	u.Data["json"] = users
	u.ServeJSON()
}

// @Title Get
// @Description get user by uid
// @Param	uid		path 	int64	true		"The key for staticblock"
// @Success 200 {object} models.User
// @Failure 403 :uid is empty
// @router /:uid [get]
func (u *UserController) Get() {
	uid, err := u.GetInt64(":uid") //strconv.ParseInt(u.GetString(":uid"), 10, 0)
	fmt.Println(uid)
	if err == nil {
		user, err := models.GetUser(uid)
		if err != nil {
			u.Data["json"] = err.Error()
		} else {
			u.Data["json"] = user
		}
	}
	u.ServeJSON()
}

// @Title Update
// @Description update the user
// @Param	uid		path 	string	true		"The uid you want to update"
// @Param	body		body 	models.User	true		"body for user content"
// @Success 200 {object} models.User
// @Failure 403 :uid is not int
// @router /:uid [put]
func (u *UserController) Put() {
	uid, err := strconv.ParseInt(u.GetString(":uid"), 10, 0)
	if err == nil {
		var user models.User
		json.Unmarshal(u.Ctx.Input.RequestBody, &user)
		uu, err := models.UpdateUser(uid, &user)
		if err != nil {
			u.Data["json"] = err.Error()
		} else {
			u.Data["json"] = uu
		}
	}
	u.ServeJSON()
}

// @Title Delete
// @Description delete the user
// @Param	uid		path 	string	true		"The uid you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 uid is empty
// @router /:uid [delete]
func (u *UserController) Delete() {
	uid, _ := strconv.ParseInt(u.GetString(":uid"), 10, 0)
	models.DeleteUser(uid)
	u.Data["json"] = "delete success!"
	u.ServeJSON()
}

// @Title Login
// @Description Logs user into the system
// @Param	username		query 	string	true		"The username for login"
// @Param	password		query 	string	true		"The password for login"
// @Success 200 {string} login success
// @Failure 403 {object}
// @router /login [post]
func (u *UserController) Login() {
	username := u.GetString("username")
	password := u.GetString("password")
	fmt.Println(username)

	//user := models.User{
	//	Username: username,
	//	Password: password,
	//}
	user, ok := models.Authenticate(username, password)  // Check username and password
	if !ok {
		u.Data["json"]  = Response{
			Success: false,
			Data: ErrUsrnameOrPasswd,
		}

		u.ServeJSON()
	}
	et := utilities.EasyToken{
		Username: user.Username,
		Uid:      user.Id,
		// TODO(jmagady): change to seconds as jwt validation uses seconds not nano
		Expires:  time.Now().Add(1 * time.Hour).UnixNano() / (int64(time.Millisecond)/int64(time.Nanosecond)),
	}

	token, err := et.GetToken()
	if token == "" || err != nil {
		u.Data["json"] = Response{
			Success: false,
			Data: ErrResponse{-0, err},
		}
	} else {
		u.Data["json"] = Response{
			Success: true,
			Data: models.LoginToken{
				User: *user,
				Token: token,
				Expires: et.Expires,
			},
		}
	}


	u.ServeJSON()
}

// @Title logout
// @Description Logs out current logged in user session
// @Success 200 {string} logout success
// @router /logout [get]
func (u *UserController) Logout() {
	u.Data["json"] = "logout success"
	u.ServeJSON()
}

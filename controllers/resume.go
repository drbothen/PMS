package controllers

import (
	"github.com/astaxie/beego"
	"repo.theoremforge.com/me/PMS/models"
	"repo.theoremforge.com/me/PMS/utilities"
	"strings"
	"fmt"
)

var (
	ErrLoadResume     = ErrResponse{500001, "failed to load file"}

)

// Resume related operations
type ResumeController struct {
	BaseController
}

// Wrapper for our returned API for Security
type Data struct {
	Data interface{}
}

// @Title GetAll
// @Description get all Parts of the Resume
// @Success 200 {object} models.Resume
// @router / [get]
func (r *ResumeController) GetAll() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		//var d Data
		//d.Data = resume
		r.Data["json"] = Response{
			Success: true,
			Data: resume,
		}
	}
	r.ServeJSON()
}

// @Title GetAllBasics
// @Description get all the basics from the resume
// @Success 200 {object} models.Basics
// @router /basics [get]
func (r *ResumeController) GetAllBasics() {
	et := utilities.EasyToken{}
	authtoken := strings.TrimSpace(r.Ctx.Request.Header.Get("Authorization"))
	valido, err := et.ValidateToken(authtoken)
	fmt.Println(valido)


	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Basics,
		}
	}
	r.ServeJSON()
}

// @Title GetProfiles
// @Description get profiles listed on the resume
// @Success 200 {object} models.Profiles
// @router /basics/profiles [get]
func (r *ResumeController) GetProfiles() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Basics.Profiles,
		}
	}
	r.ServeJSON()
}

// @Title GetLocation
// @Description get the listed location from the resume
// @Success 200 {object} models.Location
// @router /basics/location [get]
func (r *ResumeController) GetLocation() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Basics.Location,
		}
	}
	r.ServeJSON()
}

// @Title GetWork
// @Description get all work history listed on the resume
// @Success 200 {object} models.Work
// @router /work [get]
func (r *ResumeController) GetWork() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Work,
		}
	}
	r.ServeJSON()
}

// @Title GetVolunteer
// @Description get all Volunteer history listed on the resume
// @Success 200 {object} models.Volunteer
// @router /volunteer [get]
func (r *ResumeController) GetVolunteer() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Volunteer,
		}
	}
	r.ServeJSON()
}

// @Title GetVolunteer
// @Description get all Education history listed on the resume
// @Success 200 {object} models.Education
// @router /education [get]
func (r *ResumeController) GetEducation() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Education,
		}
	}
	r.ServeJSON()
}

// @Title GetAwards
// @Description get all awards listed on the resume
// @Success 200 {object} models.Awards
// @router /awards [get]
func (r *ResumeController) GetAwards() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Awards,
		}
	}
	r.ServeJSON()
}

// @Title GetPublications
// @Description get all publication history listed on the resume
// @Success 200 {object} models.Publications
// @router /publications [get]
func (r *ResumeController) GetPublications() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Publications,
		}
	}
	r.ServeJSON()
}

// @Title GetSkills
// @Description get all Skills listed on the resume
// @Success 200 {object} models.Skills
// @router /skills [get]
func (r *ResumeController) GetSkills() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Skills,
		}
	}
	r.ServeJSON()
}

// @Title GetLanguages
// @Description get all Languages listed on the resume
// @Success 200 {object} models.Languages
// @router /languages [get]
func (r *ResumeController) GetLanguages() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Languages,
		}
	}
	r.ServeJSON()
}

// @Title GetInterests
// @Description get all Interests listed on the resume
// @Success 200 {object} models.Interests
// @router /interests [get]
func (r *ResumeController) GetInterests() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		var d Data
		d.Data =  resume.Interests
		r.Data["json"] = Response{
			Success: true,
			Data: resume.Interests,
		}
	}
	r.ServeJSON()
}

// @Title GetReferences
// @Description get all References listed on the resume
// @Success 200 {object} models.Interests
// @router /references [get]
func (r *ResumeController) GetReferences() {
	resume, err := models.LoadResumeFile(beego.AppConfig.String("resume"))
	if err != nil {
		r.Ctx.ResponseWriter.WriteHeader(500)
		r.Data["json"] = Response{
			Success: false,
			Data: ErrLoadResume,
		}
	} else {
		r.Data["json"] = Response{
			Success: true,
			Data: resume.References,
		}
	}
	r.ServeJSON()
}

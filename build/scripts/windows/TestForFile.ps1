Param(
  [string]$file
)

if (Test-Path -path ./$file) {
    exit 0
} else {
    exit 2
}
$packageFile = Get-Content -Raw -Path .\package.json | ConvertFrom-Json
return $packageFile.version.Trim()
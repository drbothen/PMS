﻿# ^v([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$

Param(
  [string]$vbump
)

# pull the tag list
$tagLog = git tag -l

# pull the latest tag
$currenttag = $tagLog[-1]

# regex match the version number
$currenttag -match '^v([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$'

# Attempt to up the appropriate part of the version number
if ($vbump -eq "major") {
    $newVersion =  "v{0}.{1}.{2}" -f $([int]$Matches[1] + 1), $Matches[2], $Matches[3]
} elif ($vbump -eq "minor") {
    $newVersion =  "v{1}.{0}.{2}" -f $([int]$Matches[2] + 1), $Matches[1], $Matches[3]
} elif ($vbump -eq "patch") {
    $newVersion =  "v{1}.{2}.{0}" -f $([int]$Matches[3] + 1), $Matches[1], $Matches[2]
}

# Parse the Package file
$packageFile = Get-Content -Raw -Path .\package.json | ConvertFrom-Json

# Bump the version number in package.json
$packageFile.version = $newVersion

# write updated package file back to file
$packageFile | ConvertTo-Json  | set-content .\package.json -Force
package utilities

import (
	"net"
	"net/http"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/casbin/casbin"
	"strings"
)

// Package authz provides handlers to enable ACL, RBAC, ABAC authorization support.
// Simple Usage:
//	import(
//		"github.com/astaxie/beego"
//
//		"github.com/casbin/casbin"
//	)
//
//	func main(){
//		// mediate the access for every request
//		beego.InsertFilter("*", beego.BeforeRouter, authz.NewAuthorizer(casbin.NewEnforcer("authz_model.conf", "authz_policy.csv")))
//		beego.Run()
//	}
//
//
// Advanced Usage:
//
//	func main(){
//		e := casbin.NewEnforcer("authz_model.conf", "")
//		e.AddRoleForUser("alice", "admin")
//		e.AddPolicy(...)
//
//		beego.InsertFilter("*", beego.BeforeRouter, authz.NewAuthorizer(e))
//		beego.Run()
//	}

// NewAuthorizer returns the authorizer.
// Use a casbin enforcer as input
func NewAuthorizer(e *casbin.Enforcer) beego.FilterFunc {
	return func(ctx *context.Context) {
		a := &BasicAuthorizer{enforcer: e}

		if !a.CheckPermission(ctx.Request) {
			a.RequirePermission(ctx.ResponseWriter)
		}
	}
}

// BasicAuthorizer stores the casbin handler
type BasicAuthorizer struct {
	enforcer *casbin.Enforcer
}

// ParseToken gets the JWT from the request.
// If the token is not valid it returns an error
func (a *BasicAuthorizer) ParseToken(r *http.Request) (EasyToken, error) {
	et := EasyToken{}  // Create an empty Token
	authtoken := strings.TrimSpace(r.Header.Get("Authorization"))  // get the authorization key
	if authtoken == ""{  // if no authorization key
		et.Username = "*"  // assign to default group
		return et, nil  // return key
	}
	_, err := et.ValidateToken(authtoken)  // Validate Token
	if err != nil {  // if an invalid token
		return et, err // if ValidateToken errors, return err
	}

	return et, nil  // return token
}

// GetUserName gets the user name from the request.
// Currently, only HTTP basic authentication is supported
func (a *BasicAuthorizer) GetUserName(r *http.Request) string {
	username, _, _ := r.BasicAuth()
	return username
}

// getIpAddress gets the remote addr from the request.
func getIPAddress(r *http.Request) (string, error) {
	addr, err := net.ResolveTCPAddr("tcp", r.RemoteAddr)
	if err != nil {
		return "", err
	}
	return addr.IP.String(), nil
}

// CheckPermission checks the user/method/path combination from the request.
// Returns true (permission granted) or false (permission forbidden)
func (a *BasicAuthorizer) CheckPermission(r *http.Request) bool {
	//user := a.GetUserName(r)  // Old Method
	//TODO(jmagady): add default username on failure
	token, err := a.ParseToken(r)
	if err != nil {
		return false
	}
	user := token.Username  // Set username
	addr, err := getIPAddress(r)  // Set the requesting IP Address
	if err != nil {
		return false
	}
	method := r.Method
	path := r.URL.Path
	return a.enforcer.Enforce(user, addr, path, method)
}

// RequirePermission returns the 403 Forbidden to the client
func (a *BasicAuthorizer) RequirePermission(w http.ResponseWriter) {
	w.WriteHeader(403)
	w.Write([]byte("403 Forbidden\n"))
}

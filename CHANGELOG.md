#### 1.0.0 (2018-04-26)

##### Build System / Dependencies

* **package.json:**  added package.json required for generate-changelog ([24e0ba0d](https://repo.theoremforge.com/jmagady/mypms/commit/24e0ba0d53847325e98077f1b267ec2f8ce79e84))
* **Taskfile:**  removed Commit hash from git tag ([6bb05c2b](https://repo.theoremforge.com/jmagady/mypms/commit/6bb05c2b876d4d446e0692e8e428f7725afe8e11))

##### Chores

* **IDE:**
  *  Added markdown histroy to exclude ([5b33bcb9](https://repo.theoremforge.com/jmagady/mypms/commit/5b33bcb9d9dd3457b83f1bee302723321e783657))
  *  Fix IDE when pulling updates ([76d61902](https://repo.theoremforge.com/jmagady/mypms/commit/76d61902d0981f8b415025a4d26e0c7e8f4e5990))
* **gitignore:**  Added npm-debug log to ignore ([561cfb33](https://repo.theoremforge.com/jmagady/mypms/commit/561cfb3327ea1efac751b47e76a6b537b42d1f05))
* **.idea:**  required IDE settings ([0b1bb721](https://repo.theoremforge.com/jmagady/mypms/commit/0b1bb7218ae5e546d086bd6aadf278d61f55cefb))
* **update:**  Added Tasker ([1b4b129f](https://repo.theoremforge.com/jmagady/mypms/commit/1b4b129f8f2976a187ff437c3afcb58e77f6301b))
* ***:**
  *  Catchup refactor ([9f73a06c](https://repo.theoremforge.com/jmagady/mypms/commit/9f73a06c8d21b040108cfec3cb6d81c5a6c189fd))
  *  repo catchup on refactor ([31674a6c](https://repo.theoremforge.com/jmagady/mypms/commit/31674a6cb4cbce43a80bb3fe22340f45c96a3d7d))
  *  moved new code into dev ([8798199d](https://repo.theoremforge.com/jmagady/mypms/commit/8798199d4a55716d5114c3f727173abd96914685))
  *  Moved original code to archive ([6aa9b9b2](https://repo.theoremforge.com/jmagady/mypms/commit/6aa9b9b205f663b13c9d5941188a30582e888356))
* **README.md:**  Updated Readme ([5bd5fd18](https://repo.theoremforge.com/jmagady/mypms/commit/5bd5fd18498949e8b729f17cb8c701f3c8b666ce))

##### Continuous Integration

* **task:**  configured task yml ([c5cb6c1f](https://repo.theoremforge.com/jmagady/mypms/commit/c5cb6c1f9dec5a72f54f99428cab274039b2aa11))

##### Documentation Changes

* **README.md:**  Added initial readme with helpful dev section ([b7461d53](https://repo.theoremforge.com/jmagady/mypms/commit/b7461d53eb9a2a096809bb26a22e876faed97297))
* **CONTRIBUTING.md:**  Added contributing guide ([e9c33879](https://repo.theoremforge.com/jmagady/mypms/commit/e9c33879a119c75b386f00db018ef25de84c83d3))

##### New Features

* ***:**
  *  Moved app to static/app folder ([c7b3da3b](https://repo.theoremforge.com/jmagady/mypms/commit/c7b3da3bac808ffdd75637a2a889744c33052218))
  *  Started Resume ([d3b44a8d](https://repo.theoremforge.com/jmagady/mypms/commit/d3b44a8d6a75eeeedfdbc0d6ff7f7872891e5bdb))
  *  Changed port ([a4d5da26](https://repo.theoremforge.com/jmagady/mypms/commit/a4d5da26251614344c054963b89c77ef6fc4b5de))
  *  Testing new port ([6a66e171](https://repo.theoremforge.com/jmagady/mypms/commit/6a66e17183b7a46e3db6df06febaa47efadb685b))
  *  Simplified Import paths ([831e9f40](https://repo.theoremforge.com/jmagady/mypms/commit/831e9f40443230337a4445a832f076d37d116be8))
  *  Initial Sync ([bce0a95b](https://repo.theoremforge.com/jmagady/mypms/commit/bce0a95bded34797ea126cea329044d92568a135))
* **profile:**  Profile adjustments ([a8ef1395](https://repo.theoremforge.com/jmagady/mypms/commit/a8ef13951b5821f0612ee11cca8fcea492416b78))
* **jsonresume:**  Update resume.json ([ff51f7e2](https://repo.theoremforge.com/jmagady/mypms/commit/ff51f7e207b8f3e296b688f600f54fe5296c8f70))
* **.gitlab-ci.yml:**
  *  Deploy to top level domain ([f35e3a46](https://repo.theoremforge.com/jmagady/mypms/commit/f35e3a4648db8dde50c2d17fe49c2a392b4038f3))
  *  Added auto devops CI config ([25029313](https://repo.theoremforge.com/jmagady/mypms/commit/25029313e0e6b4925e69a58021381a6ce9d137a9))
* **app.conf:**  updated port to 80 ([ee88ee21](https://repo.theoremforge.com/jmagady/mypms/commit/ee88ee21e13f6d786f2671a1ed23decc2436f929))
* **tests:**  moved test from test folder ([628b01bf](https://repo.theoremforge.com/jmagady/mypms/commit/628b01bfbad82c0eb49ff67267d7a83af6827541))
* **go-JSONResume:**  Opensourced library ([71c30d71](https://repo.theoremforge.com/jmagady/mypms/commit/71c30d716c4e7eb40cb1bfefd4439462d24bf66c))
* **ignore:**
  *  Updated ignore file ([29d801fc](https://repo.theoremforge.com/jmagady/mypms/commit/29d801fc2e5bdd62b971ec9542805963b4245bc7))
  *  Gitignore added ([de4d002f](https://repo.theoremforge.com/jmagady/mypms/commit/de4d002fe87c18267f2a582ab6ea37d014b8a764))
* **Gopkg:**  Attempted CI fix ([aee810e1](https://repo.theoremforge.com/jmagady/mypms/commit/aee810e1855f243b4e066843c1710c3a03159b1b))
* **DEP:**  Switched package manager ([eb6a5e2c](https://repo.theoremforge.com/jmagady/mypms/commit/eb6a5e2c42296108f967fa4756e9fbc196964096))

##### Bug Fixes

* **Profile:**  Corrected Profile page ([0150f5bf](https://repo.theoremforge.com/jmagady/mypms/commit/0150f5bf9a97cc610047a07641ef96ab538373b0))
* **resume.json:**
  *  Corrected Postal Code ([a6149704](https://repo.theoremforge.com/jmagady/mypms/commit/a6149704f87072dc373add05613d2ebdf8b48cd0))
  *  corrected linkedin link ([06d25bb9](https://repo.theoremforge.com/jmagady/mypms/commit/06d25bb9ce8327a5f430b9e363c3afd5cd0e80b4))
* ***:**  fixed profile template ([73e6a94c](https://repo.theoremforge.com/jmagady/mypms/commit/73e6a94c8c09ae33bdf7da176f523941c1010e85))
* **profile.html:**
  *  corrected languages ([264425c1](https://repo.theoremforge.com/jmagady/mypms/commit/264425c17f97bcd7dace17c5714341ec19b5cf5d))
  *  corrected profile html ([fc37b41c](https://repo.theoremforge.com/jmagady/mypms/commit/fc37b41c875fd1d2c2eb1b7417ce142d935b1cc1))
* **app.conf:**  Corrected resume location ([ca038dc1](https://repo.theoremforge.com/jmagady/mypms/commit/ca038dc1d8fb26aebb2c70f65c57be5cd00b08d3))
* **test:**  Fixed tests ([f8895077](https://repo.theoremforge.com/jmagady/mypms/commit/f8895077137c6cfb5f789d8fda27b817b7fef315))
*  Fixed import of old resume package ([ef20a804](https://repo.theoremforge.com/jmagady/mypms/commit/ef20a8043e842a553934b7605a3bbacbafa4d506))
* **lock:**  fixed dep lock file ([61aaf136](https://repo.theoremforge.com/jmagady/mypms/commit/61aaf13698fcd38d04215f151bf6f0e90a364424))
* **dep:**  fixed dep issues ([14903549](https://repo.theoremforge.com/jmagady/mypms/commit/14903549b90b5a1ff1bc9c58f0c0adf8378acc5f))

##### Other Changes

* **main.go:**  Added version var ([c82a8a9a](https://repo.theoremforge.com/jmagady/mypms/commit/c82a8a9ae264143d7095298069ba06418b86cec5))
* ***:**  removed unneeded files ([9dcf7af3](https://repo.theoremforge.com/jmagady/mypms/commit/9dcf7af39a97e1cb32c37381548d0741021aaeb4))

##### Tests

* **test:**  removed test file ([b4577851](https://repo.theoremforge.com/jmagady/mypms/commit/b4577851175e9bd70372c8d303cbe44f33dd45b2))
* **imports:**  testing new import path.. ([01eaff70](https://repo.theoremforge.com/jmagady/mypms/commit/01eaff708ce83aca980545783d903798640e70ec))

